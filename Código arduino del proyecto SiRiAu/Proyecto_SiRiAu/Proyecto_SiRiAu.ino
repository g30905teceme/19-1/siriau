/* SISTEMA DE RIEGO AUTOMÁTICO CON SENSOR DE HUMEDAD DE SUELO "SiRiAu" */
int relay = 10; 
//Se declara la variable relay de la minibomba y lo asocio al pin 10
int water_bomb_speed = 100;
//Velocidad de la minibomba de agua oscila entre 100 como mínimo y 255 como máximo.8
void setup() {
  Serial.begin(9600);
}

void loop() {
  // Mide la humedad de la maceta en % y muestra el resultado
  Serial.print("Valor del sensor de humedad:");
  int humedad = map(analogRead(A0), 1023, 0, 100, 0);
  Serial.print("Humedad: ");
  Serial.print(humedad);
  Serial.println("%");
  Serial.println("*******************************");
//**************************************************************
// Condiciones de riego 
// Si la humedad en el suelo es igual o inferior al 50%, entonces el sistema de riego se activa. 
// En caso de que no se cumpla la condición anterior, el sistema de riego no se activa.
//**************************************************************
  if(humedad<= 50){
    digitalWrite(relay, HIGH);
    Serial.println("Regar");
    analogWrite(relay, water_bomb_speed);
    }
  else{
    digitalWrite(relay, LOW);
    Serial.println("No Regar");
    }
  delay(100);
}
